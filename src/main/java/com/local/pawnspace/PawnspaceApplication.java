package com.local.pawnspace;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PawnspaceApplication {

	public static void main(String[] args) {
		SpringApplication.run(PawnspaceApplication.class, args);
	}

}
