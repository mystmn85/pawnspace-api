package com.local.pawnspace.coffe.impl;

import com.local.pawnspace.model.Coffee;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface CoffeeRepositoryImpl extends CrudRepository<Coffee, String> {
}
