package com.local.pawnspace.component;

import com.local.pawnspace.coffe.impl.CoffeeRepositoryImpl;
import com.local.pawnspace.model.Coffee;
import jakarta.annotation.PostConstruct;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class DataLoader {
    private final CoffeeRepositoryImpl coffeeRepository;

    public DataLoader(CoffeeRepositoryImpl coffeeRepository){
        this.coffeeRepository = coffeeRepository;
    }

    @PostConstruct
    private void loadData(){
        coffeeRepository.saveAll(List.of(
                new Coffee("Café Cereza"),
                new Coffee("Café Ganador"),
                new Coffee("Café Lareño"),
                new Coffee("Café Três Pontas")
        ));
    }
}
