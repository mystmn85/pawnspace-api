package com.local.pawnspace.config;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
public class GreetingConfig {
    public GreetingConfig(){}
    @Value("${app.greeting-name: Mirage}")
    private String name;

    public String getName(){
        return name;
    }
}
