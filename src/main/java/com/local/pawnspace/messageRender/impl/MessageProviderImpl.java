package com.local.pawnspace.messageRender.impl;

public interface MessageProviderImpl {
    String getMessage();
}
