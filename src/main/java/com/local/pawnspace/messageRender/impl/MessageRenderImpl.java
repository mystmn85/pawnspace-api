package com.local.pawnspace.messageRender.impl;

public interface MessageRenderImpl {
    void render();
    void setMessageProvider(MessageProviderImpl messageProvider);
    MessageProviderImpl getMessageProvider();
}
