package com.local.pawnspace.model;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import lombok.*;

import java.util.UUID;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Coffee {
    @Id
    private String id;
    private String name;

    public void setId(String id){
        this.id = id;
    }

    public Coffee(String name){
        this(UUID.randomUUID().toString(), name);
    }
}
