package com.local.pawnspace;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@Disabled
@SpringBootTest
class PawnspaceApplicationTests {

	@Test
	void contextLoads() {
	}

}
